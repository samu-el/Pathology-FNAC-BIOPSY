<?php 

require_once '../core/init.php';

if (Input::exists()) {
        $name = Input::get('name');
        $sex = Input::get('sex');
        $age = Input::get('age');
        $hospital = Input::get('hospital');
        $doctors = Input::get('doctors');
        $previous = Input::get('previous');
        $sampled = Input::get('sampled');
        $clinic = Input::get('clinic');
        $clicnical_diagnosis = Input::get('clicnical_diagnosis');
        $specimen = Input::get('specimen');
        $microscopy = Input::get('microscopy');
        $diagnosis = Input::get('diagnosis');
        $comment = Input::get('comment');
        $db = DB::getInstance();
        if (!$db->insert(
                'table_name',
                array(

                        'name' => $name,
                        'sex' => $sex,
                        'age' => $age,
                        'hospital' => $hospital,
                        'doctor' => $doctors,
                        'previous' => $previous,
                        'sampled' => $sampled,
                        'clinic' => $clinic,
                        'clicnical_diagnosis' => $clicnical_diagnosis,
                        'specimen' => $specimen,
                        'microscopy' => $microscopy,
                        'diagnosis' => $diagnosis,
                        'comment' => $comment
                )
        )) {
                throw new Exception('Sorry, there was a problem inserting the data ');
        }
};
?>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
    <title>FNAC Report</title>
</head>
<body>
    <div class="container">
        <div class="row align-items-center justify-content-center">
                <div class="col-md-2" id="hideSubmit">
                         <input type="button" class="form-control btn-primary" name="FNAC" value="Biopsy Report" onClick="document.location.href='index.php'">
                </div>
		<div class="col-md-6">
			<h1 class="text-center">FNAC Report</h1>
		</div>
		<div class="col-md-1"></div>
                <div class="col-md-3">
                        <p class="bottom-marigin">Addis Ababa Univesity</p>
                        <p class="bottom-marigin">CHS School of Medicine</p>
                        <p class="bottom-marigin">Department of Pathology</p>
                </div>
	</div>
	<hr>
	<div class = "form">
            <form>
                <div class="form-group row">
                    <label for="Name" class="col-md-1">Name</label>
                    <input type="text" class="form-control col-md-4" id="Name" name="name" required="required">
                    <div class="col-md-2"></div>
                    <h2 class="col-md-2"><input type="number" class="form-control font-weight-bold" name="number" value=<?php echo date("Y"); ?> required="required" style="font-size:24px;"></h2>
                    <h2 class="col-md-2"><input type="number" class="form-control font-weight-bold" name="number" value=1 required="required" style="font-size:24px;"></h2>
                </div>
                <div class="form-group row">
                        <label for="sex" class="col-md-1">Sex</label>
                        <select class="form-control col-md-4">
                                <option>M</option>
                                <option>F</option>
                        </select>
                        <label for="Age" class="col-md-1">Age</label>
                        <input type="number" class="form-control col-md-1" name="age" value="" required="required">
                </div>
                <div class="form-group row">
                        <label for="Hospital" class="col-md-1">Hospital</label>
                        <input type="text" class="form-control col-md-4" name="hospital" value="Tikiur Anbessa Hospital" required="required">
                </div>
                <div class="form-group row">
                        <label for="Name" class="col-md-1">Doctor</label>
                        <input type="text" class="form-control col-md-4" name="doctor" required="required">
                </div>
                <br>
                <div class="form-group row">
                        <label for="Name" class="col-md-1">Previous</label>
                        <input type="text" class="form-control col-md-4" name="previous" required="required">
                        <div class="col-md-2"></div>
                        <label for="sampled" class="col-md-1">Sampled</label>
                        <input type="text" class="form-control col-md-3" name="sampled" value="" required="required">
                </div>
                <div class="form-group row">
                        <label for="Name" class="col-md-1">Clinic</label>
                        <input type="text" class="form-control col-md-4" name="clinic" required="required">
                </div>
                <div class="form-group row">
                        <label for="Name" class="col-md-1">Clinical Diagnosis</label>
                        <input type="text" class="form-control col-md-4" name="clicnical_diagnosis" required="required">
                </div>
                <br>
                <div class="form-group row">
                        <label for="Name" class="col-md-1">Specimen</label>
                        <input type="text" class="form-control col-md-4" name="specimen" required="required">
                </div>
                <div class="form-group row">
                        <label for="Name" class="col-md-1">Microscopy</label>
                        <input type="text" class="form-control col-md-4" name="microscopy" required="required">
                </div>
                <div class="form-group row">
                        <label for="Name" class="col-md-1">Diagnosis</label>
                        <input type="text" class="form-control col-md-4" name="diagnosis" required="required">
                </div>
                <div class="form-group row">
                        <label for="Name" class="col-md-1">Comment</label>
                        <input type="text" class="form-control col-md-4" name="comment" required="required">
                </div>
                <div class="form-group row" id="hideSubmit">
                        <label for="Name" class="col-md-1"></label>
                        <input type="submit" class="form-control col-md-2 btn-success" name="submit" value="Submit">
                        <div class="col-md-1"></div>
                        <input type="button" class="form-control col-md-2 btn-primary" name="print" value="Print" onClick="window.print();">
                </div>
            </form>
        </div>
        <br><br>
        <footer class="footer">
                        <div class="container row">
                                <div class="col-md-4">Date: <?php echo date("d-M-Y"); ?></div>
                                <div class="col-md-4">Resident Dr. </div>
                                <div class="col-md-4">Pathologist</div>
                        </div>
        </footer>        
    </div>
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/tether.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
